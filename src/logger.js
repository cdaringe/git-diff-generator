'use strict'

const app = require('./app')
const toArray = require('lodash.toarray')

module.exports = {
    _log: function(level, args) {
        args = toArray(args)
        const output = ['[git-differ]', `${level.toUpperCase()}:`]
          .concat(args)
          .map(i => (i === undefined || i === null) ? '' : i.toString())
          .join(' ')
        if (level === 'warn' || level == 'error') {
          console.error(output)
        } else {
          console.log(output)
        }
    },

    error: function() {
      this._log('error', arguments)
    },

    verbose: function() {
      if (!app.verbose) return
      this._log('verbose', arguments)
    }
}
