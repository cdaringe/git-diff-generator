/**
 * @module GitDiffGenerator
 */
'use strict'

const app = require('./app')
const logger = require('./logger')
const cp = require('child_process')
const path = require('path')

module.exports = {
  buildDiffCmd: function(commit1, commit2, ndx) {
    const format = app.format || '%ndx-%commit1...%commit2.diff'
    const filename = format
      .replace('%ndx', ndx)
      .replace('%commit1', commit1)
      .replace('%commit2', commit2)
    const cmd = `git diff --full-index ${commit1}...${commit2} > ${path.join(app.out, filename)}`
    logger.verbose(`running diff: ${cmd}`)
    return cmd
  },

  /**
   * logs settings.
   * @returns GitDiffGenerator
  */
  logSettings: function() {
    logger.verbose('SETTINGS')
    for (var i in app) {
      if (['_logSettings', 'verbose', 'commits'].indexOf(i) === -1) logger.verbose('\t', `${i}:`, app[i])
    }
    return this
  },

  processCommits: function() {
    app.commits.forEach((commit, ndx) => {
      if (!ndx) return // no prev commit
      const cmd = this.buildDiffCmd(commit, app.commits[ndx - 1], app.commits.length - ndx)
      cp.execSync(cmd, { cwd: app.repo })
    })
  }
}