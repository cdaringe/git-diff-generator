#!/usr/bin/env node

'use strict'
const app = require('./app')
const main = require('./index')
const cli = require('./cli')

main
  .logSettings()
  .processCommits()