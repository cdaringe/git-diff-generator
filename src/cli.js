'use strict'

const pkg = require('../package.json')
const program = require('commander')
const path = require('path')
const fs = require('fs')
const app = require('./app')
const logger = require('./logger')

program
  .version(pkg.version)
  .option('-c, --config [file]', 'commit data to parse')
  .option('-o, --out [dir]', 'output dir. defaults to cwd')
  .option('-r, --repo [path]', 'path to repo. otherwise, defaults to cwd')
  .option('-v, --verbose', 'get loud')
  .parse(process.argv)

if (program.verbose) app.verbose = true
if (!program.config) {
  logger.error('please provide a config file input')
  program.help()
  process.exit(1)
}

try {
  Object.assign(app, JSON.parse(fs.readFileSync(program.config)))
} catch (err) {
  logger.error("unable to read commit file :(")
  throw err
}

if (program.repo) {
  try {
    fs.lstatSync(program.repo)
  } catch (err) {
    logger.error('unable to read repo dir')
    throw err
  }
  app.repo = program.repo
}

if (program.out) {
  try {
    fs.lstatSync(program.out)
  } catch (err) {
    logger.error('unable to find output dir')
    throw err
  }
  app.out = program.out
}

module.exports = program
