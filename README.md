# git-diff-generator

## what

generate a series of diffs, provided a list of inorder commits.

## install

`npm install -g git-diff-generator`

## usage

of course, you can see your options from the cli
```
$ git-diff-generator -h

  Usage: git-diff-generator [options]

  Options:

    -h, --help            output usage information
    -V, --version         output the version number
    -c, --config [file]   config // <== MOST IMPORTANT!
    -o, --out [dir]       output dir. defaults to cwd
    -r, --repo [path]     path to repo. otherwise, defaults to cwd
    -v, --verbose         get loud
```

```json
// config.json
{
  // REQUIRED
  // we will `git diff --full-index commit1...commit2 > /out/path/formatted-filename.dff
  // ordering: latest commits on top/first, oldest commits on bottom/last. just as out of `git log`
  "commits": [
    "d63bf42",
    "a99bc56"
  ],
  // OPTIONAL
  // file output format accepts the following template inputs
  // %ndx, index of the diff in the set of commited diffs (1 indexed)
  // %commit1, earlier commit
  // %commit2, later commit
  "format": "%ndx-%commit1...$commit2.diff" 
}
```

## example

from this repo itself, i can perform the following:

```bash
$ git-diff-generator -c test/input/this-repo-config.json -o test/output/ -v
[git-differ] VERBOSE: SETTINGS
[git-differ] VERBOSE:  	 out: test/output/
[git-differ] VERBOSE:  	 repo: /Users/cdieringer/node/git-diff-generator
[git-differ] VERBOSE:  	 format: %ndx-%commit1...%commit2.diff
[git-differ] VERBOSE: running diff: git diff --full-index a99bc56...d63bf42 > test/output/1-a99bc56...d63bf42.diff
```

sure enough, `test/output/1-a99bc56...d63bf42.diff` is ready to go!
